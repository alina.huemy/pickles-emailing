## Pickles Auction Emailing Service

This emailing service is developed using NestJS with microservice and Apache Kafka. The email service is a consumer that is subscribed to the topic 'send.email'. 

With this, microservices can be created as the producer that publishes to the topic.

![Pickles](Pickles.png)

## Running the app

To clone this project, run:

```bash
git clone https://gitlab.com/alina.huemy/pickles-emailing.git
```

To run Apache Kafka, ensure that you have Docker installed into your machine. Go to the project directory and run:

```bash
docker-compose up
```

To run the email service project, please *cd* into **email-backend** path and run

```bash
# install dependencies
$ npm install

# run the app
$ npm run start
```

## Test

```bash
# unit tests
$ npm run test
```

## Test Producer App

Created a test producer project called **email-producer** to test out the functionality.

 You can check that out, change your email address at **app.controller.ts** *line 19* and start it like how you started the **email-backend** project.

## Compromises/Shortcut

- Values should not be hardcoded, better to put into .env or configuration files
- Should use nodemailer for more sophisticated email service (E.g. can add attachment, use template) if there is a SMTP server configured. I am using SendGrid for simplicity sake.
- Test coverage can be improved, no end to end testing.