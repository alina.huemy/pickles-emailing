import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {ClientsModule, Transport} from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'order_service',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'order_client',
            brokers: ['localhost:29092'],
          },
          consumer: {
            groupId: 'order_group',
          },
        },
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
