import { Controller, Get, Post, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientKafka } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
    @Inject('order_service') private readonly client: ClientKafka) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('email-test')
  testEmail(){
    return this.client.emit(
      'send.email', {
        to:'alina.huemy@gmail.com', //replace this email add
        subject: 'Test Subject', 
        message: 'This is the content of the email'});
  }
}
