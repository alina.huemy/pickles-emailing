import { Controller, Get, PreconditionFailedException } from '@nestjs/common';
import { AppService } from './app.service';
import {
  Ctx,
  KafkaContext,
  MessagePattern,
  Payload,
} from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,) {}

  @MessagePattern('send.email')
   async sendEmail(@Payload() message: any, @Ctx() context: KafkaContext){
    const originalMessage = context.getMessage();
    const messageValue = originalMessage.value;

    if(messageValue == null || messageValue == undefined){
      throw new PreconditionFailedException();
    }
    const content = JSON.parse(JSON.stringify(messageValue));

    return await this.appService.sendEmail(content);
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
