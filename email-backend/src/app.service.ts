import { Injectable, HttpException, HttpStatus, Param } from '@nestjs/common';
import { SendGridService } from "@anchan828/nest-sendgrid";
import { ConfigService } from '@nestjs/config';
import { EmailContent } from './dto/email-content';

@Injectable()
export class AppService {
  constructor(private readonly sendGrid: SendGridService,
    private readonly configService: ConfigService){}

  async sendEmail(content: EmailContent): Promise<string> {

    try{
      await this.sendGrid.send({
        to: content.to,
        from: this.configService.get<string>('EMAIL_SENDER'),
        subject: content.subject,
        text: content.message,
        html: content.message
      });

      const response = 'Email is sent successfully';
      console.log(response);
      
      return response;

    } catch (error){
      console.log('An error has occurred');
      throw new HttpException('Error in sending email', HttpStatus.BAD_REQUEST);
    }
  }

  getHello(): string {
    return 'Hello World!';
  }
}
