import { SendGridModule, SendGridService } from '@anchan828/nest-sendgrid';
import { ConfigService } from '@nestjs/config';
import { KafkaContext } from '@nestjs/microservices';
import { KafkaMessage } from '@nestjs/microservices/external/kafka.interface';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

class SendGridServiceMock {}

describe('AppController', () => {
  let appController: AppController;
  let spyService: AppService;

  const args = ['test', { test: true }];
  let context: KafkaContext;

  beforeEach(async () => {
    context = new KafkaContext(args as [KafkaMessage, number, string]);

    const SendGridServiceProvider = {
      provide: SendGridService,
      useClass: SendGridServiceMock,
    };

    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, ConfigService, SendGridServiceProvider],
    }).compile();

    appController = app.get<AppController>(AppController);
    spyService = app.get<AppService>(AppService);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
  });

  describe('send email', () => {
    it('should call sendEmail to send an email', async () => {
      const message = 'any';
      const spy = jest.spyOn(spyService, 'sendEmail').
                  mockReturnValue(Promise.resolve('Email is sent successfully'));
      await expect(appController.sendEmail(message, context)).toThrowError;
    })
  });

});
