import { IsEmail, IsNotEmpty } from 'class-validator';

export class EmailContent {
    @IsEmail()
    @IsNotEmpty()
    to: string;

    @IsNotEmpty()
    subject: string;
    
    @IsNotEmpty()
    message: string;
} 