import { SendGridService } from '@anchan828/nest-sendgrid';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from './app.service';

class SendGridServiceMock {
    send(_content: any) {}
}

describe('AppService', () => {
    let appService: AppService;

    beforeEach(async () => {
        const SendGridServiceProvider = {
            provide: SendGridService,
            useClass: SendGridServiceMock,
        };

        const module: TestingModule = await Test.createTestingModule({
            providers: [AppService, SendGridServiceProvider, ConfigService],
          }).compile();

        appService = module.get<AppService>(AppService);
    });

    it('AppService - should be defined', () => {
        expect(appService).toBeDefined();
    });

    describe('Send Email', () => {
        it('should send an email', async () => {
          const content = {
              to: 'alina.huemy@gmail.com',
              subject: 'This is a test subject',
              message: 'This is the email content'
          };
          const response = await appService.sendEmail(content);
          await expect(response).toEqual('Email is sent successfully');
        });
    });

    describe('Empty field validation', () => {
        it('should return error if field is empty', async () => {
          const content = {
              to: undefined,
              subject: 'This is a test subject',
              message: 'This is the email content'
          };
          const response = await appService.sendEmail(content);
          await expect(response).toThrowError;
        });
    });

    describe('Email field validation', () => {
        it('should return error if field is empty', async () => {
          const content = {
              to: 'test',
              subject: 'This is a test subject',
              message: 'This is the email content'
          };
          const response = await appService.sendEmail(content);
          await expect(response).toThrowError;
        });
    });
});