import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SendGridModule } from "@anchan828/nest-sendgrid";
import { ConfigModule } from '@nestjs/config';
import configuration from './config/configuration';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'email_service',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'email_client',
            brokers: ['localhost:29092'],
          },
          consumer: {
            groupId: 'email_group',
          },
        },
      },
    ]),
    SendGridModule.forRoot({
      apikey: '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',
    }),
    ConfigModule.forRoot({
      load: [configuration],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
